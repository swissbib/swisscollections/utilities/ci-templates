# sbt Build Template

This template extends the `test` and `build` stages with capabilities to test and
package (via the `assembly` plugin) a sbt project. By the end of the `build` stage,
you can download the packaged project as artifact from `target/scala-2.13/<your-app-name>.jar`.

## Usage

This template can directly be included in a project:

for scala 2.13

```yaml
# .gitlab-ci.yml

include:
  - project: 'swissbib/swisscollections/utilities/ci-templates'
    file: 'sbt-build/sbt-build-2.13.yml'
# (...)
```

In order to speed up the building process, enable the caching of dependencies by
including the following snippet in your `.gitlab-ci.yml`:

```yaml
cache:
  # use this key to share among all commits in the branch (cf. https://docs.gitlab.com/ee/ci/caching/#share-caches-between-jobs-in-the-same-branch)
  key: "$CI_COMMIT_REF_SLUG"
  paths:
    - "sbt-cache/ivy/cache"
    - "sbt-cache/boot"
    - "sbt-cache/sbtboot"
    - "sbt-cache/target"
```
