# Status December 6th 2024

Not used any more, switch to CI/CD components https://gitlab.switch.ch/ub-unibas/ci


# ci-templates

This repository contains a set of Gitlab CI templates ("`.gitlab-ci.yml`
files"). These templates can either be copied and slightly adapted to match
the specific needs of the respective project. Or you can directly
[include](https://docs.gitlab.com/ee/ci/yaml/README.html#include) them in the
`.gitlab-ci.yml` definitions of actual swisscollections services. 

The templates are designed to be composable. This means you normally combine
several of the templates to cover the necessary CI steps (building, testing,
creating 
artifacts / publishing). To meet this purpose, the templates already have a
certain stage defined where the respective job should be run:

- `test`: In this stage all the testing happens.
- `build`: Used to create a productive build of the project. This step is
  often included in the `publish` stage if the artifact is the only entity to
be deployed.
- `publish`: For building artifacts like Docker images.
- `deploy`: Not yet used.

In this sense the templates can be seen extensions to any stage.

## Templates Registry

Find further information on the templates in their respective directory.

- [Testing and building sbt-based projects](./sbt-build/)
- [Publishing Docker images](./docker-image/)
- [Testing and publishing Helm charts](./helm-chart/)


## Test CI definition

Gitlab provides a service to test `.gitlab-ci.yml` files if they are
syntactically correct. For checking, paste the content of the definition into
[this
form](https://gitlab.switch.ch/swissbib/swisscollections/utilities/ci-templates/-/ci/lint).

## Further resources

- [More on the CI/CD workflow of the Memobase
  project](https://memobase.atlassian.net/wiki/spaces/TBAS/pages/45154536)
- [Gitlab CI/CD reference](https://docs.gitlab.com/ee/ci/yaml/)
- [Dockerfile reference](https://docs.docker.com/engine/reference/builder/)
