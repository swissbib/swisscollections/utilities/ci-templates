# Docker Image Template

This template can directly be included in a project:

```sh
# .gitlab-ci.yml

include:
  - project: 'swissbib/swisscollections/utilities/ci-templates'
    file: 'docker-image/docker-image.yml'

# (...)
```
